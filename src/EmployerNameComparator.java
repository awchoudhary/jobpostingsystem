import java.util.Comparator;

/**
 * Defines a comparator that is used to sort the names of employers in a list of employer objects alphabetically
 * @author Awaes Choudhary
 * awaes.choudhary@stonybrook.edu
  */
public class EmployerNameComparator implements Comparator<Employer> {
	
	/**
	 * The following method returns -1 if the first argument come alphabetically before the second, 0 if they are the same and 1 otherwise.
	 * @return -1 or 0 or 1
	 */
	public int compare(Employer first, Employer second){
		return first.getEmployerName().compareToIgnoreCase(second.getEmployerName());
	}
	
	
}
