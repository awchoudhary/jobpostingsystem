
import java.io.Serializable;
import java.util.ArrayList;

/**
 * The following class extends the ArrayList class and represents a list of jobPostings for one employer
 * @author Awaes Choudhary
 * awaes.choudhary@stonybrook.edu
 */
public class Employer extends ArrayList<JobPosting> implements Serializable  {
	String employerName;
	
	/**
	 * accessor for the name of the employer
	 * @return String name 
	 */
	public String getEmployerName(){
		return employerName;
	}
	
	/**
	 * mutator for the name of the employer
	 * @param newEmployerName
	 */
	public void setEmployerName(String newEmployerName){
		employerName = newEmployerName;
	}
}