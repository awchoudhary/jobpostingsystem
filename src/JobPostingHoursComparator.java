import java.util.Comparator;

/**
 * Defines a comparator that is used to sort JobPosting objects according to their hours per week.
 * @author Awaes Choudhary
 * awaes.choudhary@stonybrook.edu
*/
public class JobPostingHoursComparator implements Comparator<JobPosting>{
	
	/**
	 * The following method returns -1 if the first argument is less than  the second, 0 if they are the same and 1 otherwise.
	 * @return -1 or 0 or 1
	 */
	public int compare(JobPosting first, JobPosting second){
		if(first.getHoursPerWeek() > second.getHoursPerWeek()){
			return 1;
		}
		else if(first.getHoursPerWeek() == second.getHoursPerWeek()){
			return 0;
		}
		else{
			return -1;
		}
	}

}

