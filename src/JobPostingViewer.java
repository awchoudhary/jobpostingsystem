import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * The following class loads an ArrayList of employers, if employers.obj is found or creates a new list and prompts the user by printing a menu, 
 * after which it performs various operations based on the options that the user inputs. Upon exiting, the class saves the list of employers to employers.obj
 * @author Awaes Choudhary
 * awaes.choudhary@stonybrook.edu
*/
public class JobPostingViewer {
	public static void main(String[] args) throws ClassNotFoundException{
		List<Employer> employers = deserialize("employers.obj");
		Scanner input = new Scanner(System.in);
		for(;;){
			System.out.println("\nA) Add Employer\n");
			System.out.println("B) Add Job\n");
			System.out.println("C) Remove Employer\n");
			System.out.println("D) Remove Job\n");
			System.out.println("E) Sort and print Employers\n");
			System.out.println("F) Sort and print Jobs\n");
			System.out.println("Q) Quit\n");
			String option = input.nextLine();
			if(option.equalsIgnoreCase("a")){
				System.out.println("Enter the employer’s name: ");
				String employerName = input.nextLine();
				Employer newEmployer = new Employer();
				newEmployer.setEmployerName(employerName);
				employers.add(newEmployer);
				System.out.println("Added employer " + employerName);
			}
			else if(option.equalsIgnoreCase("b")){
				System.out.println("Enter the number of the employer: ");
				int employerNumber = Integer.parseInt(input.nextLine());
				employerNumber--;
				System.out.println("Enter the job title: ");
				String jobName = input.nextLine();
				System.out.println("Enter the salary: ");
				int salary = Integer.parseInt(input.nextLine());
				System.out.println("Enter the number of hours per week: ");
				int hours = Integer.parseInt(input.nextLine());
				System.out.println("Enter the number of vacation days per year: ");
				int vacDays = Integer.parseInt(input.nextLine());
				JobPosting a = new JobPosting(jobName, salary, hours, vacDays);
				employers.get(employerNumber).add(a);
				System.out.println("Added job " + jobName + " to employer " + employers.get(employerNumber).getEmployerName());
			}
			else if(option.equalsIgnoreCase("c")){
				System.out.println("Enter the number of the employer: ");
				int employerNumber = Integer.parseInt(input.nextLine());
				employerNumber--;
				String tempEmpName = employers.get(employerNumber).getEmployerName();
				employers.remove(employerNumber);
				System.out.println("Removed employer " + tempEmpName);
			}
			else if(option.equalsIgnoreCase("d")){
				System.out.println("Enter the number of the employer: ");
				int numEmp = Integer.parseInt(input.nextLine());
				numEmp--;
				System.out.println("Enter the number of the job to remove: ");
				int jobNum = Integer.parseInt(input.nextLine());
				jobNum --;
				String tempJobName = employers.get(numEmp).get(jobNum).getJobTitle();
				employers.get(numEmp).remove(jobNum);
				System.out.println("Removed job " + tempJobName + " from employer " + employers.get(numEmp).getEmployerName());
			}
			else if(option.equalsIgnoreCase("e")){
				System.out.println("\nA) Sort by name\nB) Sort by number of postings");
				String option2 = input.nextLine();
				if(option2.equalsIgnoreCase("a")){
					Collections.sort(employers, new EmployerNameComparator());
					printEmployers(employers);
				}
				else if(option2.equalsIgnoreCase("b")){
					Collections.sort(employers, new EmployerNumJobsComparator());
					printEmployers(employers);
				}
			}
			else if(option.equalsIgnoreCase("f")){
				System.out.println("Enter employer number: ");
				int empNum = Integer.parseInt(input.nextLine());
				empNum --;
				System.out.println("\nA) Sort by name\nB) Sort by salary\nC) Sort by hours per week\nD) Sort by vacation days per year\nE) Sort by custom preference score: if the user selects this option, prompt for the three weights\n");
				String option2 = input.nextLine();
				if(option2.equalsIgnoreCase("a")){
					Collections.sort(employers.get(empNum), new JobPostingNameComparator());
					printEmployer(employers.get(empNum));
				}
				else if(option2.equalsIgnoreCase("b")){
					Collections.sort(employers.get(empNum), new JobPostingSalaryComparator());
					printEmployer(employers.get(empNum));
				}
				else if(option2.equalsIgnoreCase("c")){
					Collections.sort(employers.get(empNum), new JobPostingHoursComparator());
					printEmployer(employers.get(empNum));
				}
				else if(option2.equalsIgnoreCase("d")){
					Collections.sort(employers.get(empNum), new JobPostingVacationComparator());
					printEmployer(employers.get(empNum));
				}
				else if(option2.equalsIgnoreCase("e")){
					System.out.println("Enter the salary weight: ");
					int salaryWeight = Integer.parseInt(input.nextLine());
					System.out.println("Enter the hours/week weight: ");
					int hoursWeight = Integer.parseInt(input.nextLine());
					System.out.println("Enter the vacations days/year weight: ");
					int vacationWeight = Integer.parseInt(input.nextLine());
					Collections.sort(employers.get(empNum), new JobPostingWeightedComparator(salaryWeight, hoursWeight, vacationWeight));
					printEmployer(employers.get(empNum));
				}
				
			}
			else if(option.equalsIgnoreCase("q")){
				serialize("employers.obj", employers);
				System.out.println("Saved employers to “employers.obj”");
				System.exit(0);
			}
			else{
				System.out.println("Invalid option selected...");
			}
		}
	}
	
	/**
	 * Prints out the list of employers in the form of a table
	 * @param List<Employer> a
	 */
	public static void printEmployers(List<Employer> a){
		System.out.println("#   Employer               # Jobs");
		System.out.println("--  --------               ------");
	    for(int i = 1; i <= a.size(); i++){
			System.out.printf("%-3d %-20s %6d\n", i, a.get(i - 1).getEmployerName(), a.get(i - 1).size());
		}
	}
	
	/**
	 * Prints out the JobPostings in a single employer list in the form of a table
	 * @paramList<JobPosting> a
	 */
	public static void printEmployer(List<JobPosting> a){
		System.out.println("#   Job Title                   Salary   Hours   Vacation");
		System.out.println("--  ---------                   ------   -----   --------");
		for(int i = 1; i <= a.size(); i++){
			System.out.printf("%-3d %-23s %9d %7d %8d\n", i, a.get(i - 1).getJobTitle(), a.get(i - 1).getSalary(), a.get(i - 1).getHoursPerWeek(), a.get(i - 1).getVacationDaysPerYear());
		}
	}
	
	/**
	 * Saves the employers list into the file name specified by the parameter. 
	 * @param filename
	 * @param List<Employer> a
	 */
	public static void serialize(String filename, List<Employer> a){
		try {
			FileOutputStream file = new FileOutputStream(filename);
			ObjectOutputStream fout = new ObjectOutputStream(file);
			fout.writeObject(a);
			fout.close();
		} catch (IOException e){

		}
	}
	
	/**
	 * Loads the list of employers stored inside the specified file name, or creates a new list if the file is not found. 
	 * @param filename
	 * @return List<Employer> myEmployers
	 * @throws ClassNotFoundException
	 */
	public static List<Employer> deserialize(String filename) throws ClassNotFoundException{
		List<Employer> myEmployers = null;
		try {
			FileInputStream file = new FileInputStream(filename);
			ObjectInputStream fin  = new ObjectInputStream(file);
			myEmployers = (List<Employer>) fin.readObject(); 
			fin.close(); 
			System.out.println("Loaded employers from “employers.obj”");

		} catch(IOException e){ 
			System.out.println("“employers.obj” not found. Using new employers.");
			myEmployers = new ArrayList<Employer>();
		}
		return myEmployers;
	}
}

