import java.util.Comparator;

/**
 * Defines a comparator that is used to sort JobPosting objects alphabetically according to their jobTitles.
 * @author Awaes Choudhary
 * awaes.choudhary@stonybrook.edu
*/
public class JobPostingNameComparator implements Comparator<JobPosting> {
	
	/**
	 * The following method returns -1 if the first argument come alphabetically before the second, 0 if they are the same and 1 otherwise.
	 * @return -1 or 0 or 1
	 */
	public int compare(JobPosting first, JobPosting second){
		return first.getJobTitle().compareToIgnoreCase(second.getJobTitle());
	}
}

