import java.util.Comparator;

/**
 * Defines a comparator that compares jobPostings by a user defined weighted sum of salary, hours per week and vacation days per year.
 * @author Awaes Choudhary
 * awaes.choudhary@stonybrook.edu
 * CSE 214 Fall Rec.04
 * Fall 2014 Homework #7
 * Recitation TA: Anthony Musco
 */
public class JobPostingWeightedComparator implements Comparator<JobPosting> {
	int salaryWeight;
	int hoursWeight;
	int vacationWeight;
	
	/**
	 * Creates an instance of the comparator object with default valles for the following parameters. 
	 * @param newSalaryWeight
	 * @param newHoursWeight
	 * @param newVacationWeight
	 */
	public JobPostingWeightedComparator(int newSalaryWeight, int newHoursWeight, int newVacationWeight){
		salaryWeight = newSalaryWeight;
		hoursWeight = newHoursWeight;
		vacationWeight = newVacationWeight;
	}
	
	/**
	 * Calculates the preferences for both parameters based on the weights for each variable as inputed by the user and returns -1 if the preference for the 
	 * first JobPosting is less than that of the second, 0 if both preferences are equal and 1 otherwise. 
	 */
	public int compare(JobPosting first, JobPosting second){
		int preferenceFirst = (first.getSalary() * salaryWeight) + (first.getHoursPerWeek() * hoursWeight) + (first.getVacationDaysPerYear() * vacationWeight);
		int preferenceSecond = (second.getSalary() * salaryWeight) + (second.getHoursPerWeek() * hoursWeight) + (second.getVacationDaysPerYear() * vacationWeight);
		if(preferenceFirst < preferenceSecond){
			return -1;
		}
		else if(preferenceFirst == preferenceSecond){
			return 0;
		}
		else{
			return 1;
		}
	}
}

