import java.io.Serializable;

/**
 * The following class creates a JobPosting object which keeps track of the job title, salary, hours per week and the vacation days per year
 * for the job
 * @author Awaes Choudhary
 * awaes.choudhary@stonybrook.edu
  */
public class JobPosting implements Serializable {
	String jobTitle;
	int salary;
	int hoursPerWeek;
	int vacationDaysPerYear;
	
	/**
	 * Creates an instance of the JobPosting class and takes in the following parameters.
	 * @param newJobTitle
	 * @param newSalary
	 * @param newHoursPerWeek
	 * @param newVacationDaysPerYear
	 */
	public JobPosting(String newJobTitle, int newSalary, int newHoursPerWeek, int newVacationDaysPerYear){
		jobTitle = newJobTitle;
		salary = newSalary;
		hoursPerWeek = newHoursPerWeek;
		vacationDaysPerYear = newVacationDaysPerYear;
	}
	
	/**
	 * accessor method for the JobTitle variable
	 * @return jobTitle String
	 */
	public String getJobTitle(){
		return jobTitle;
	}
	
	/**
	 * mutator for the jobTitle variable
	 * @param newJobTitle
	 */
	public void setJobTitle(String newJobTitle){
		jobTitle = newJobTitle;
	}
	
	/**
	 * accessor for the hoursPerWeek variable
	 * @return int hoursPerWeek
	 */
	public int getHoursPerWeek(){
		return hoursPerWeek;
	}
	
	/**
	 * mutator for the hoursPerWeek variable
	 * @param newHoursPerWeek
	 */
	public void setHoursPerWeek(int newHoursPerWeek){
		hoursPerWeek = newHoursPerWeek;
	}
	
	/**
	 * accessor for the salary variable
	 * @return int salary
	 */
	public int getSalary(){
		return salary;
	}
	
	/**
	 * mutator for the salary variable
	 * @param newSalary
	 */
	public void setSalary(int newSalary){
		salary = newSalary;
	}
	
	/**
	 * accessor for the vacationDaysPerYear variable
	 * @return int vacationDaysPerYear
	 */
	public int getVacationDaysPerYear(){
		return vacationDaysPerYear;
	}
	
	/**
	 * mutator for the vacationDaysPerYear variable
	 * @param newVacationDaysPerYear
	 */
	public void setVacationDaysPerYear(int newVacationDaysPerYear){
		vacationDaysPerYear = newVacationDaysPerYear;
	}
}
