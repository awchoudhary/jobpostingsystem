import java.util.Comparator;

/**
 * Defines a comparator that is used to sort the number of jobPostings of employers in a list of employer objects.
 * @author Awaes Choudhary
 * awaes.choudhary@stonybrook.edu
  */
public class EmployerNumJobsComparator implements Comparator<Employer> {
	
	/**
	 * The following method returns -1 if the first argument is less than  the second, 0 if they are the same and 1 otherwise.
	 * @return -1 or 0 or 1
	 */
	public int compare(Employer first, Employer second){
		if(first.size() < second.size()){
			return -1;
		}
		else if(first.size() == second.size()){
			return 0;
		}
		else{
			return 1;
		}
	}
}

